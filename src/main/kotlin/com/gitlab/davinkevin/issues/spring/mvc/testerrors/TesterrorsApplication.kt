package com.gitlab.davinkevin.issues.spring.mvc.testerrors

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException
import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse
import org.springframework.web.servlet.function.router

@SpringBootApplication
class TesterrorsApplication

fun main(args: Array<String>) {
	runApplication<TesterrorsApplication>(*args)
}

@Configuration
@Import(Handler::class)
class Router {

	@Bean
	fun routes(h: Handler) = router {
		GET("gone", h::notFound)
	}
}

class Handler {
	fun notFound(r: ServerRequest): ServerResponse = throw ResponseStatusException(HttpStatus.GONE, "sorry…")
}