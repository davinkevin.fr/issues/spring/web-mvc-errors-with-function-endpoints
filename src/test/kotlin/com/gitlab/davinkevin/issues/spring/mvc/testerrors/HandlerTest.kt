package com.gitlab.davinkevin.issues.spring.mvc.testerrors

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.ImportAutoConfiguration
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.context.annotation.Import
import org.springframework.http.HttpStatus
import org.springframework.test.web.reactive.server.WebTestClient

@WebMvcTest(controllers = [Handler::class])
@Import(Router::class)
@ImportAutoConfiguration(ErrorMvcAutoConfiguration::class)
class HandlerTest(
    @Autowired val rest: WebTestClient
) {

    @Test
    fun `should return gone with details`() {
        /* Given */
        /* When */
        val v = rest
            .get()
            .uri("/gone")
            /* Then */
            .exchange()
            .expectStatus()
            .isEqualTo(HttpStatus.GONE)
            .expectBody()
            .returnResult()
            .responseBody
            ?.let(::String)!!

        /* Then */
        assertThat(v).isNotEmpty()

        /*
        Where something like this is expected:

        {
          "timestamp": "2024-04-28T14:43:57.477+00:00",
          "status": 404,
          "error": "Not Found",
          "path": "/notfound"
        }
         */
    }
}